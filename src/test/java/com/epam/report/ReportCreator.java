package com.epam.report;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.stream.Stream;


public class ReportCreator implements ITestListener {
    private Logger LOGGER = LogManager.getLogger("DaoTests");

    @Override
    public void onFinish(ITestContext context) {
        LOGGER.trace("PASSED TEST CASES");
        context.getPassedTests()
                .getAllResults()
                .stream()
                .map(result -> result.getName())
                .forEach(LOGGER::trace);
        LOGGER.trace("FAILED TEST CASES");
        context.getFailedTests()
                .getAllResults()
                .stream()
                .map(result -> result.getName())
                .forEach(LOGGER::trace);
        LOGGER.trace("Test completed on: " + context.getEndDate()
                .toString() + "____________________");
    }

    @Override
    public void onStart(ITestContext arg0) {
        LOGGER.trace("Started testing on: " + arg0.getStartDate()
                .toString());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        LOGGER.trace("Failed : " + arg0.getName());

    }

    @Override
    public void onTestSkipped(ITestResult arg0) {
        LOGGER.trace("Skipped Test: " + arg0.getName());
    }

    @Override
    public void onTestStart(ITestResult arg0) {
        Stream.of("_______________________________", "Testing: " + arg0.getName())
                .forEach(LOGGER::trace);
    }

    @Override
    public void onTestSuccess(ITestResult arg0) {
        long timeTaken = ((arg0.getEndMillis() - arg0.getStartMillis()));
        LOGGER.trace("Tested: " + arg0.getName() + " Time taken:" + timeTaken + " ms");

    }
}
