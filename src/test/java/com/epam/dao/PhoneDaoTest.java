package com.epam.dao;

import com.epam.model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.sql.SQLException;

public class PhoneDaoTest {
    private User user;

    public PhoneDaoTest() throws SQLException {
        user = new LoginDao().getUser("test", "test");
    }

    @Test
    public void callHistoryTest() {
        try {
            new PhoneDao().getCallHistory(user.getId()).stream()
                                                       .filter(call -> call.getUser_id() == user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail("problem with sql request");
        } catch (NullPointerException e) {
            e.printStackTrace();
            new PhoneDao().registerCall("test", 1, user.getId());
        }
        Assert.assertNotNull(new PhoneDao().registerCall("test", 1, user.getId()));
    }

    @Test
    public void smsHistoryTest() {
        try {
            new PhoneDao().getSmsHistory(user.getId()).stream()
                                                      .filter(message -> message.getId() == user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail("problem with sql request");
        } catch (NullPointerException e) {
            e.printStackTrace();
            new PhoneDao().registerCall("test", 1, user.getId());
        }
        Assert.assertNotNull(new PhoneDao().registerSMS("test", "test", user.getId()));
    }
}