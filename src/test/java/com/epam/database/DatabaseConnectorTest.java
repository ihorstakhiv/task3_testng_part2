package com.epam.database;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseConnectorTest {

        @Test
        public void dbConnectionValidTest() {
            Connection dbConnection = DatabaseConnector.getDBConnection();
            try {
                dbConnection.isValid(5);
            } catch (SQLException e) {
                e.getErrorCode();
                Assert.fail();
            }
        }

        @Test
        public void dbConnectionNotNullTest()  {
            Connection dbConnection = DatabaseConnector.getDBConnection();
            Assert.assertNotNull(dbConnection);
        }
    }
