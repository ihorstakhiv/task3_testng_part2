create schema if not exists networking_db;
use networking_db;

create table if not exists user (
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null,
  `surname` varchar(45) not null,
  `username` varchar(45) not null,
  `password` varchar(45) not null
  )
engine = InnoDB;

create table if not exists phone (
  `id` int not null auto_increment primary key,
  `com.epam.model` varchar(45) not null,
  `number` varchar(45) not null,
  `balance` double(255, 2) default 0.00,
  `user_id` int not null,
  `provider_id` int(25)
  )
engine = InnoDB;

create table if not exists operator (
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null,
  `call_in_network_price` double(3, 2),
  `call_out_network_price` double(3, 2),
  `sms_in_network_price` double(3, 2),
  `sms_out_network_price` double(3, 2),
  `chars_in_sms` int(5)
  )
engine = InnoDB;

create table if not exists calls (
  `id` int not null auto_increment primary key,
  `receiver` varchar(45) not null,
  `call_time` int(25),
  `user_id` int(25)
  )
engine = InnoDB;

create table if not exists message (
  `id` int not null auto_increment primary key,
  `receiver` varchar(45) not null,
  `sms_text` varchar(250),
  `user_id` int(25)
  )
engine = InnoDB;

insert into operator (name, call_in_network_price, call_out_network_price,
sms_in_network_price, sms_out_network_price, chars_in_sms) 
values
('Kyivstar', 0.10, 0.80, 0.50, 1.05, 30),
('Life', 0.05, 0.50, 0.35, 0.75, 40),
('Unknown', 1.00, 2.00, 0.50, 1.00, 25);

