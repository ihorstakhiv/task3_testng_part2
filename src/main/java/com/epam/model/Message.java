package com.epam.model;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Table(name = "message")
public class Message {
    public static final Logger logger = LogManager.getLogger(Message.class);
    @Column(name = "id")
    private int id;
    @Column(name = "receiver")
    private String receiver;
    @Column(name = "sms_text")
    private String smsText;
    @Column(name = "user_id")
    private int userID;

    public Message() { }

    public Message(String receiver, String smsText, int userID) {
        this.receiver = receiver;
        this.smsText = smsText;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void print(){
        logger.info("\n\t---------------------------------" +
                "\n\tReceiver: " + getReceiver() +
                "\n\tSms body: " + getSmsText() +
                "\n\t---------------------------------");
    }
}
