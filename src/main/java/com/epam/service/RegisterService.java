package com.epam.service;

import com.epam.dao.RegisterDao;

public class RegisterService {
    public int createUser(String name, String surname, String username, String password) {
        return new RegisterDao().createUser(name, surname, username, password);
    }

    public int createPhone(String name, String surname, String username, String password, String model, String number, int networkProviderId) {
        return new RegisterDao().createPhone(name, surname, username, password, model, number, networkProviderId);
    }

    public int getUserID(String name, String surname, String username, String password) {
        return new RegisterDao().getUserID(name, surname, username, password);
    }
}
