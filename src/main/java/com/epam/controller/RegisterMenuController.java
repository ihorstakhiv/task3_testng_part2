package com.epam.controller;

import com.epam.service.OperatorService;
import com.epam.service.RegisterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class RegisterMenuController implements Controller {
    public static final Logger logger = LogManager.getLogger(RegisterMenuController.class);
    private Scanner scanner = new Scanner(System.in);

    public void createUser() {
        logger.info("Please enter information about you:");
        logger.info("   Your name: ");
        String name = scanner.nextLine();
        logger.info("   Your surname: ");
        String surname = scanner.nextLine();
        logger.info("   Username: ");
        String username = scanner.nextLine();
        logger.info("   Password: ");
        String password = scanner.nextLine();
        int countOfUsers = new RegisterService().createUser(name, surname, username, password);
        int countOfPhones = createPhone(name, surname, username, password);
        logger.trace(String.format("%d user was created with %d mobile.\n", countOfUsers, countOfPhones));
        logger.trace("Back to Register menu.\n");
    }

    private int createPhone(String name, String surname, String username, String password) {
        logger.info("Create your Mobile:\n");
        logger.info("   Mobile model:");
        String model = scanner.nextLine();
        logger.info("   Provide Number (in format: 0XXXXXXXXX):");
        String number = scanner.nextLine();
        int networkProviderId = detectNetworkOperator(number);
        return new RegisterService().createPhone(name, surname, username, password, model, number, networkProviderId);
    }

    private int detectNetworkOperator(String number){
        return new OperatorService().detectNetworkProvider(number);
    }

    public void back() {
        logger.trace("Back to com.epam.Application menu.\n");
    }

    @Override
    public void print() {}
}
