package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WelcomeMenuController implements com.epam.controller.Controller {
    public static final Logger logger = LogManager.getLogger(WelcomeMenuController.class);

    public void quit() {
        logger.trace("You have successfully exited");
    }

    @Override
    public void print() {
    }
}
