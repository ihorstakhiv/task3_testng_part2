package com.epam.utils;

public class TimeCounter {
    private final long start;

    public TimeCounter() {
        this.start = System.currentTimeMillis();
    }

    public double getTime() {
        long now = System.currentTimeMillis();
        return (((now - start) / 1000.0));
    }
}
