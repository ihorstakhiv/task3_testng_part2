package com.epam.utils.constant;

public class MenuConstant {
    private static int MAX_WELCOME_MENU_VALUE = 3;
    private static int MAX_LOGIN_MENU_VALUE = 2;
    private static int key = -1;
    private static int MAX_USER_MENU_VALUE = 8;

    public static int getMaxWelcomeMenuValue() {
        return MAX_WELCOME_MENU_VALUE;
    }

    public static int getMaxLoginMenuValue() {
        return MAX_LOGIN_MENU_VALUE;
    }

    public static int getKey() {
        return key;
    }

    public static int getMaxUserMenuValue() {
        return MAX_USER_MENU_VALUE;
    }
}
