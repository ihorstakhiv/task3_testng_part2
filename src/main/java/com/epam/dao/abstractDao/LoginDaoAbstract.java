package com.epam.dao.abstractDao;

import com.epam.model.User;

import java.sql.SQLException;

public interface LoginDaoAbstract {
     User getUser(String username, String password) throws SQLException;
}
